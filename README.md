## Sobre o projeto

É o meu primeiro projeto em React!

Eu já usei alguma versão do Angular bem lá no início, trabalho atualmente com Vue (framework que eu conheço e uso desde os primórdios quando estava saindo de ainda uma versão v0.18...) e Svelte. Com React é a minha primeira experiência de realmente criar um projeto. Tanto que tomei esse desafio mais como uma oportunidade de estudo do que o próprio desafio. De início estava pensando em fazer em Vue, algo que me deixa extremamente confortável, mas preferi saí dessa zona de conforto.

Sobre estrutura eu realmente nem conheço padrões e estrutura de React mas pude aplicar alguns conceitos que utilizo em outros lugares:

- Com essa versão funcional do React, deixando de ser class-based, algo até que a próxima versão (3.0) do Vue vai trazer consigo, consegui aplicar algo que naturalmente eu gosto: programação funcional.

- No Vue temos os Vue template files (.vue), que são arquivos que possuem o HTML, JS e Style em um só lugar, tornando real o conceito de component. Conceito esse que gosto muito. Aqui pude aplicar um pouco desse conceito utilizando o style-components. Tal qual no Vue utilizamos o CSS modules para organizar nosso CSS, aqui preferi deixar os styled components escopado para uso interno.

- Antes do Vue, eu já aplicava uma técnica próxima disso na organização do CSS parecido com o BEM: a SUIT CSS. É uma metodologia bem legal e bem legível, evitando um possível caos no futuro e permitindo uma melhor organização e a visão de componentes no próprio HTML e no CSS, mesmo que seja apenas uma maneira de organizar.

- No JS não tenho experiência com Typescript e optei por usar o ES6.

- No design, utilizei o Adobe XD para criar o layout e tentei dar destaque às playlists. Optei por uma grande imagem e um título imponente. Para a imagem de fundo, estou usando uma API simples do Unsplash para tentar dar o efeito "imagem relacionada ao país".

Não consegui disponibilizar muito tempo para o desafio, então optei por fazer algo básico. Tentei deixar minimalista, o que não é ruim visto que é um estilo de design que particularmente eu gosto bastante, mas dado o tempo possíveis interações e acabamentos mais pontuais foram deixado de lado.

🎼

---

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
