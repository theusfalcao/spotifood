import React, { useState, useEffect, useRef } from 'react';
import styled from 'styled-components'
import './App.css'
import { getFilters, spotifyGetFeaturedPlaylists, spotifyGetMe } from './api.js'
import { Playlists } from './components/Playlists'
import { Filters } from './components/Filters'
import { Sidebar } from './components/Sidebar'
import { Main } from './components/Main'
import { Login } from './components/Login'
import { FeaturedCountry } from './components/FeaturedCountry'
import { FiltersBar } from './components/FiltersBar'

const SectionTitle = styled.h1`
  font-weight: bold;
  font-size: var(--font-size-30);
  letter-spacing: var(--character-spacing-0);
  color: var(--oh-white);
  text-transform: uppercase;
  margin-bottom: 25px;
`

export default function App() {
  const spotifyFeaturedDefault = {
    message: '',
    items: []
  }

  const [spotifyAuthCode, setSpotifyAuthCode] = useState(window.sessionStorage.getItem('spotifood_spotify_token'))

  const [filters, setFilters] = useState([])
  const [userFilters, setUserFilters] = useState({})
  const [spotifyFeatured, setSpotifyFeatured] = useState(spotifyFeaturedDefault)
  const [spotifyProfile, setSpotifyProfile] = useState({})
  const LIVE_API_TIMEOUT = useRef(0)

  const handleSpotifyResponse = (response) => {
    if (response.error) {
      if (response.error.status === 401) {
        setSpotifyAuthCode(null)
        window.sessionStorage.removeItem('spotifood_spotify_token')
      }
      return
    }

    if (!response.playlists) return

    const { message, playlists: { items } } = response
    setSpotifyFeatured({ message, items })
  }

  const handleRemoveFilter = (id) => {
    const filters = { ...userFilters }
    delete filters[id]
    setUserFilters(filters)
  }

  const handleFiltersChange = (filter) => setUserFilters({ ...userFilters, ...filter })

  const getCountryNameFromCountryCode = (countryCode) => {
    const countryScheme = filters.filter((filter) => filter.id === 'country')[0]

    if (countryScheme) {
      const countries = countryScheme.values
      const country = countries.filter((country) => country.value === countryCode)[0]

      if (country) return country.name
    }

    return countryCode
  }

  useEffect(() => {
    const runApi = () => {
      if (spotifyAuthCode) spotifyGetFeaturedPlaylists(spotifyAuthCode, userFilters).then(handleSpotifyResponse)
    }

    window.clearTimeout(LIVE_API_TIMEOUT.current)
    runApi()
    LIVE_API_TIMEOUT.current = window.setTimeout(runApi, 30000)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userFilters])

  useEffect(() => {
    getFilters().then(setFilters)

    if (window.location.hash) {
      const locationHashs = window.location.hash.split('&')
      const acessToken = locationHashs.filter((hash) => hash.indexOf('access_token='))[0]
      const [, token] = acessToken ? acessToken.split('=') : []

      if (token) {
        setSpotifyAuthCode(token)
        window.sessionStorage.setItem('spotifood_spotify_token', token)
        window.location.hash = ''
      }
    }

    const spotifyToken = spotifyAuthCode || window.sessionStorage.getItem('spotifood_spotify_token')

    if (spotifyToken) {
      Promise.all([
        spotifyGetFeaturedPlaylists(spotifyToken),
        spotifyGetMe(spotifyToken)
      ]).then(([featuredPlaylists, profile]) => {
        handleSpotifyResponse(featuredPlaylists)

        if (profile.error && profile.error.status === 401) {
          setSpotifyAuthCode(null)
          return
        }
        setSpotifyProfile(profile)
      })
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if (!spotifyAuthCode) return <Login />

  return (
    <div className="container-fluid App-container">
      <div className="row App-row">
        <Sidebar profile={spotifyProfile}>
          <Filters
            scheme={filters}
            userFilters={userFilters}
            onChange={handleFiltersChange} />
        </Sidebar>

        <Main>
          <FeaturedCountry
            title={spotifyFeatured.message}
            country={getCountryNameFromCountryCode(userFilters.country)} />

          <SectionTitle>Featured</SectionTitle>

          <FiltersBar
            scheme={filters}
            userFilters={userFilters}
            onRemove={handleRemoveFilter} />

          <Playlists items={spotifyFeatured.items} />
        </Main>
      </div>
    </div>
  )
}