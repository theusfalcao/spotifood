import React from 'react'
import styled from 'styled-components'

export const Container = styled.main`
  padding-top: 45px;
  padding-right: 45px;
  padding-left: 25px;
  background-color: var(--navy-blue);
  height: 100%;
  overflow: auto;

  @media (max-width: 768px) {
    padding-left: 25px;
    padding-right: 25px;
    flex: 0 0 100%;
    max-width: 100%;
  }
`

export function Main({ children }) {
  return (
    <Container className="col-12 col-sm-12 col-md-8 col-lg-9 col-xl-10">
      { children }
    </Container>
  )
}