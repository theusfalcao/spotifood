import styled from 'styled-components'

export const Button = styled.button`
  border: 0;
  border-radius: 4px;
  background-color: var(--pinkful);
  color: var(--oh-white);
  padding: 10px 20px;
  transition: opacity linear .15s;

  &:hover {
    opacity: .8;
  }
`