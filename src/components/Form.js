import styled from 'styled-components'

export const Input = styled.input`
  background: var(--navy-blue) 0% 0% no-repeat padding-box;
  letter-spacing: var(--character-spacing-0);
  color: var(--midnight-blue);
  border: 1px solid transparent;
  border-radius: 4px;
  width: 100%;
  height: 40px;
  padding: 0 16px;

  ::placeholder {
    opacity: .7;
    color: var(--midnight-blue);
    font: Regular 14px/19px Nunito Sans;
  }

  :-ms-input-placeholder {
    opacity: .7;
    color: var(--midnight-blue);
    font: Regular 14px/19px Nunito Sans;
  }

  ::-ms-input-placeholder {
    opacity: .7;
    color: var(--midnight-blue);
    font: Regular 14px/19px Nunito Sans;
  }

  &[type="date"]::-webkit-inner-spin-button {
      opacity: 0
  }

  &[type="date"]::-webkit-calendar-picker-indicator {
      background: url('https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/259/calendar_1f4c5.png') center/60% no-repeat;
      color: rgba(0, 0, 0, 0);
      opacity: 0.5
  }

  &[type="date"]::-webkit-calendar-picker-indicator:hover {
      background: url('https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/259/calendar_1f4c5.png') center/60% no-repeat;
      opacity: 0.8
  }

  &:focus {
    outline: none;
    border: 1px solid var(--midnight-blue-dark);
  }
`

export const Select = styled.select`
  background: var(--navy-blue);
  font-size: var(--font-size-14);
  color: var(--midnight-blue);
  border: 1px solid transparent;
  border-radius: 4px;
  width: 100%;
  height: 40px;
  padding: 0 16px;

  &:focus {
    outline: none;
    border: 1px solid var(--midnight-blue-dark);
  }
`