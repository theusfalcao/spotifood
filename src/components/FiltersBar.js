import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Bar = styled.ul`
  padding: 0;
  margin-bottom: 65px;
`

const Filter = styled.li`
  background-color: var(--dark-blue);
  color: var(--moon-blue);
  text-transform: uppercase;
  padding: 17px 23px;
  display: inline-block;
  border-radius: 90px;
  margin-right: 20px;
  margin-bottom: 20px;

  button {
    background: transparent;
    border: 0;
    margin-left: 17px;
  }

  svg {
    fill: var(--pinkful);
  }
`

export function FiltersBar({ scheme, userFilters, onRemove }) {
  const [activeFilters, setActiveFilters] = useState([])

  const getFilterByID = (id) => scheme.filter((filter) => filter.id === id)[0]

  const handleRemove = (id) => onRemove(id)

  const getValue = (value, id) => {
    if (id === 'timestamp') return String(value).slice(0, 10)
    return value
  }

  useEffect(() => {
    const filters = Object
      .keys(userFilters)
      .map((id) => {
        const filter = getFilterByID(id)

        return {
          id,
          value: userFilters[id],
          name: filter.name
        }
      })

    setActiveFilters(filters)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [scheme, userFilters])

  return (
    <Bar>
      { activeFilters.map((filter) =>
        <Filter
          key={filter.id}>
          {filter.name}: <b>{getValue(filter.value, filter.id)}</b>
          <button onClick={() => handleRemove(filter.id)}>
            <svg xmlns="http://www.w3.org/2000/svg" width="22.4" height="22.4" viewBox="0 0 22.4 22.4">><path d="M19.2,8A11.2,11.2,0,1,0,30.4,19.2,11.2,11.2,0,0,0,19.2,8Zm0,20.232A9.032,9.032,0,1,1,28.232,19.2,9.03,9.03,0,0,1,19.2,28.232Zm4.6-11.841L20.988,19.2,23.8,22.009a.542.542,0,0,1,0,.768L22.777,23.8a.542.542,0,0,1-.768,0L19.2,20.988,16.391,23.8a.542.542,0,0,1-.768,0L14.6,22.777a.542.542,0,0,1,0-.768L17.412,19.2,14.6,16.391a.542.542,0,0,1,0-.768L15.623,14.6a.542.542,0,0,1,.768,0L19.2,17.412,22.009,14.6a.542.542,0,0,1,.768,0L23.8,15.623a.542.542,0,0,1,0,.768Z" transform="translate(-8 -8)"/></svg>
          </button>
        </Filter>
      ) }
    </Bar>
  )
}

FiltersBar.defaultProps = {
  values: [],
  userFilters: {},
  onRemove: Function
}

FiltersBar.propTypes= {
  values: PropTypes.array,
  userFilters: PropTypes.object,
  onRemove: PropTypes.func
}