import React, { useEffect, useState } from 'react'
import { Input, Select } from './Form.js'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const FilterGroup = styled.div`
  margin-bottom: 16px;
`

const FilterTitle = styled.h4`
  font-size: var(--font-size-16);
  letter-spacing: var(--character-spacing-0);
  color: var(--moon-blue);
  text-transform: uppercase;
`

export function Filter({ id, name, values, value, validation, onChange }) {
  const [inputType, setInputType] = useState('text')
  const [inputValidation, setInputValidation] = useState({ min: null, max: null})

  useEffect(() => {
    if (validation) {
      if (validation.entityType === 'DATE') setInputType('date')
      if (validation.entityType === 'DATE_TIME') setInputType('date')
      if (validation.primitiveType === 'INTEGER') setInputType('number')
      if (validation.min || validation.max) setInputValidation({
        min: validation.min ? validation.min : null,
        max: validation.max ? validation.max : null
      })
    }
  }, [validation])

  const handleOnChange = (e) => {
    const val = inputType === 'date' ? `${e.target.value}T09:00:00` : e.target.value
    onChange(val, e)
  }

  const getValue = (value, inputType) => {
    if (inputType === 'date') return String(value).slice(0, 10)
    return value
  }

  if (values.length) {
    return (
      <FilterGroup>
        <FilterTitle>{name}</FilterTitle>
        <Select
          onChange={handleOnChange}
          value={value}
          name={id}>
          <option
            value=""
            style={{ display: 'none' }}>{name}</option>
          { values.map((option) =>
            <option
              key={option.value}
              value={option.value}>{ option.name }</option>
          )}
        </Select>
      </FilterGroup>
    )
  }

  return (
    <FilterGroup>
      <FilterTitle>{name}</FilterTitle>
      <Input
        key={id}
        min={inputValidation.min}
        max={inputValidation.max}
        type={inputType}
        onChange={handleOnChange}
        placeholder={name}
        value={getValue(value, inputType)}
        name={id} />
    </FilterGroup>
  )
}

Filter.defaultProps = {
  values: [],
  validation: undefined,
  onChange: Function
}

Filter.propTypes= {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  validation: PropTypes.object,
  values: PropTypes.array,
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ])
}