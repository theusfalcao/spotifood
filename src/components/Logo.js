import React from 'react'
import styled from 'styled-components'
import logo from '../assets/logo.png'

const SpotifoodLogo = styled.img`
  width: 100%;
  max-width: 229px;
  display: block;
  margin: 0 auto;
`

export function Logo() {
  return (
    <SpotifoodLogo src={logo} />
  )
}