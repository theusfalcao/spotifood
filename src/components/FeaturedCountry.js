import React from 'react'
import styled from 'styled-components'

const Title = styled.h1`
  position: relative;
  letter-spacing: 0px;
  color: var(--oh-white);
  font-size: var(--font-size-94);
  font-weight: 700;
  word-break: break-word;
  text-align: center;
`

const Featured = styled.div`
  display: flex;
  position: relative;
  background-image: url('https://source.unsplash.com/1600x900/?${props => props.country ? 'country,' + props.country : 'music'}');
  border-radius: 30px;
  height: 484px;
  width: 100%;
  background-size: cover;
  background-position: center center;
  align-items: center;
  justify-content: center;
  box-shadow: 0px 40px 99px #28365D;
  margin-bottom: 90px;
  padding: 50px;

  @media (max-width: 992px) { height: 350px; }
  @media (max-width: 768px) {
    height: 250px;
    margin-bottom: 45px;
  }

  &::before {
    content: '';
    background-color: #000;
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    border-radius: 30px;
    opacity: .25;
  }
`

export function FeaturedCountry({ country, title }) {
  return (
    <Featured country={country}>
      <Title>{title}</Title>
    </Featured>
  )
}