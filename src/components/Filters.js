import React from 'react'
import { Filter } from './Filter'
import PropTypes from 'prop-types'

export function Filters({ scheme, userFilters, onChange }) {

  const handleChange = (val, e) => onChange({ [e.target.name]: val })

  return (
    <>
      { scheme.map((scheme) =>
        <Filter
          onChange={handleChange}
          key={scheme.id}
          id={scheme.id}
          name={scheme.name}
          values={scheme.values}
          value={userFilters[scheme.id] ? userFilters[scheme.id] : ''}
          validation={scheme.validation} />
      ) }
    </>
  )
}

Filters.defaultProps = {
  values: [],
  userFilters: {},
  onChange: Function
}

Filters.propTypes= {
  values: PropTypes.array,
  userFilters: PropTypes.object,
  onChange: PropTypes.func
}