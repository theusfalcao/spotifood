import React from 'react'
import styled from 'styled-components'

const PlaylistCover = styled.img`
  border-radius: 15px;
  width: 100%;
`

const Playlist = styled.div`
  margin-bottom: 30px;

  a {
    text-decoration: none;
  }
`

const PlaylistsItems = styled.div``

const PlaylistDescription = styled.h2`
  font-weight: bold;
  font-size: var(--font-size-25);
  letter-spacing: var(--character-spacing-0);
  color: var(--midnight-blue);
  text-align: center;
  margin-top: 25px;
`

export function Playlists({ items }) {
  const removeHTML = (html) => html.replace(/<[^>]+>/g, '')

  return (
    <PlaylistsItems className="row">
      { items.map((item) =>
        <Playlist
          key={item.id}
          className="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3">
          <a
            href={item.external_urls.spotify}
            target="_blank"
            rel="noopener noreferrer">
            <PlaylistCover
              src={item.images[0].url} />
            <PlaylistDescription>{removeHTML(item.description)}</PlaylistDescription>
          </a>
        </Playlist>
      ) }
    </PlaylistsItems>
  )
}