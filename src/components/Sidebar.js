import React, { useState } from 'react'
import styled from 'styled-components'
import { Logo } from './Logo'

const Aside = styled.aside`
  background-color: var(--dark-blue);
  padding: 50px 35px 50px 35px;
  justify-content: space-between;
  display: flex;
  flex-direction: column;
  overflow: auto;
  height: 100%;

  @media (max-width: 768px) {
    display: none;
  }
`

const HeaderMobile = styled.div`
  display: none;
  padding: 20px;
  background-color: var(--dark-blue);

  @media (max-width: 768px) {
    display: block;
  }
`

const Content = styled.div`
  ${props => props.visible === false ? 'display: none' : null};
`

const Profile = styled.div`
  text-align: center;
`

const ProfileImage = styled.img`
  box-shadow: 0px 14px 17px #15203F;
  border-radius: 191px;
  width:  ${props => props.small ? '40px' : '118px;'};
  height: ${props => props.small ? '40px' : '118px;'};
`

const ProfileName = styled.h1`
  font-weight: bold;
  font-size: var(--font-size-25);
  letter-spacing: var(--character-spacing-0);
  color: var(--oh-white);
  letter-spacing: 0px;
  margin-top: 16px;
`

const SidebarTitle = styled.h3`
  font-weight: bold;
  font-size: var(--font-size-25);
  letter-spacing: var(--character-spacing-0);
  color: var(--pinkful);
  text-transform: uppercase;
  margin-bottom: 30px;

  @media (max-width: 768px) {
    margin-top: 30px;
  }
`

const LogoContainer = styled.div`
  margin-bottom: 30px;

  ${props => props.small ? `
    margin-bottom: 0;
    img {
      width: 130px;
    }
  ` : null}
`

const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const HeaderMenu = styled.div`
  display: flex;
`

const FilterButton = styled.button`
  margin-right: 15px;
  border: 0;
  border-radius: 4px;
  background: ${props => props.active ? 'var(--midnight-blue-dark)' : 'transparent'};

  svg {
    fill: ${props => props.active ? 'var(--pinkful)' : 'var(--oh-white)'};
  }
`

export function Sidebar({ children, profile }) {
  const [isFilterMobileVisible, setIsFilterMobileVisible] = useState(false)

  const handleFilterButtonClick = () => setIsFilterMobileVisible(!isFilterMobileVisible)

  const spotifyName = profile && profile.display_name
  const defaultImage = `https://avatar.oxro.io/avatar.svg?name=${spotifyName}&background=526390&length=1`
  const spotifyImage = profile && profile.images && profile.images.length ? profile.images[0].url : defaultImage

  return (
    <>
      <HeaderMobile className="col-12">
        <Header>
          <LogoContainer small>
            <Logo />
          </LogoContainer>

          <HeaderMenu>
            <FilterButton
              active={isFilterMobileVisible}
              onClick={handleFilterButtonClick}>
              <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M10 18h4v-2h-4v2zM3 6v2h18V6H3zm3 7h12v-2H6v2z"/></svg>
            </FilterButton>
            { profile && (
              <ProfileImage
                small
                src={spotifyImage} />
            )}
          </HeaderMenu>
        </Header>

        <Content visible={isFilterMobileVisible}>
          <SidebarTitle>Filters</SidebarTitle>
          {children}
        </Content>
      </HeaderMobile>

      <Aside className="col-auto col-md-4 col-lg-3 col-xl-2">

        <Content>
          <SidebarTitle>Filters</SidebarTitle>
          {children}
        </Content>

        { profile && (
          <Profile>
            <ProfileImage src={spotifyImage} />
            <ProfileName>{spotifyName}</ProfileName>
          </Profile>
        )}
      </Aside>
    </>
  )
}

