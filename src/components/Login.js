import React from 'react'
import { goToSpotifyAuthorize } from '../api.js'
import styled from 'styled-components'
import { Logo } from './Logo'
import { Button } from './Button'

const Main = styled.main`
  display: flex;
  height: 100%;
  background-color: var(--navy-blue);
  align-items: center;
  justify-content: center;
  text-align: center;
  padding: 30px;
`

const H1 = styled.h1`
  color: var(--midnight-blue);
  font-weight: bold;
  margin: 60px 0;
  font-size: 30px;
`

export function Login() {
  return (
    <Main>
      <div>
        <Logo />
        <H1>Login with Spotify to continue</H1>
        <Button onClick={goToSpotifyAuthorize}>
          Login
        </Button>
      </div>
    </Main>
  )
}