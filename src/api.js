export function goToSpotifyAuthorize() {
  const queryString = []
  queryString.push(`response_type=token`)
  queryString.push(`client_id=${process.env.REACT_APP_SPOTIFY_CLIENT_ID}`)
  queryString.push(`redirect_uri=${window.location.href}`)

  const endpoint = `https://accounts.spotify.com/authorize?${queryString.join('&')}`

  window.location.href = endpoint
}

export function getFilters() {
  return new Promise((resolve) =>
    fetch('https://www.mocky.io/v2/5a25fade2e0000213aa90776')
      .then((response) => response.json())
      .then(({ filters }) => resolve(filters || []))
      .catch((e) => resolve([])))
}

export function spotifyGetFeaturedPlaylists(Authorization, filters) {
  const query = filters ? Object.entries(filters).reduce((queryString, [query, value]) => queryString += `${query}=${value}&`, '') : ''

  return fetch(`https://api.spotify.com/v1/browse/featured-playlists?${query}`, {
    headers: {
      Authorization: `Bearer ${Authorization}`
    }
  }).then((response) => response.json())
}

export function spotifyGetMe(Authorization) {
  return fetch(`https://api.spotify.com/v1/me`, {
    headers: {
      Authorization: `Bearer ${Authorization}`
    }
  }).then((response) => response.json())
}